<?php
include("./lib/function.php");
include("./lib/setting.php");

if( isset($_POST) && count($_POST) ){
	writeLog($_POST);
	header('Location: index.php');
	exit;
}

echo <<< EOM
<link rel="stylesheet" type="text/css" href="./css/style.css" />
<div>
	<form action="./index.php" method="post">
		<dl>
		<dd>タイトル</dd>
		<dt><input type="text" name="title" /></dt>
		<dd>本文</dd>
		<dt><textarea name="body" rows="5" cols="100"></textarea></dt>
		</dl>
		<p><input type="submit" value="書き込み" /></p>
	</form>
</div>
EOM;

$logs = showLog();
	
if($logs){
	$index = 1;
	echo "<div>";
	foreach( $logs as $log ){
		foreach( $log as $line ){
			$list = explode( "\t", $line );
			$title = $list[0];
			$body = $list[1];
			$time = $list[2];
			echo "<p><strong>".$index.". ".$title."</strong> ".$time."</p>";
			echo "<p>".$body."</p>";
			$index++;
		}
	}
	echo "</div>";
}

?>
