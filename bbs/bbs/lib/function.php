<?php 

/**
* ログの書き込みを行う
*
* @param array $params['title'] : タイトル
*              $params['body'] : 本文
* @return なし
*/
function writeLog($params)
{
	$title = $params["title"];
	$body = str_replace("\n","", nl2br($params["body"]));
	$logStr = sprintf("%s\t%s\t%s\r\n", $title, $body, date("Y/m/d H:i") );
	$fname = D_LOG_DIR.date("Ymd").".tsv";
	
	$logFP = fopen( $fname, "a" ) ;
	if ( $logFP ) {
		fwrite( $logFP, $logStr ) ;
	}
	fclose( $logFP ) ;
}

/**
* ログの読み出しを行う
*
* @return array ログ一覧配列
*/
function showLog()
{
	$ret = null;
	
	if ($dir = opendir(D_LOG_DIR)) {
		while (($file = readdir($dir)) !== false) {
			if ($file != "." && $file != "..") {
				$ret[] = file(D_LOG_DIR.$file) ;
			}
		} 
		closedir($dir);
	}
	
	return $ret;
}

?>
